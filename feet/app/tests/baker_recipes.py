from datetime import datetime
from model_bakery.recipe import Recipe, foreign_key
from app.models import (
    Runner,
    Info,
    Device,
    Activity,
    Report,
    Route,
)
from django.utils.timezone import make_aware

example_runner = Recipe(
    Runner,
    first_name="runner_name",
    last_name="runner_lastname",
    created_at=make_aware(datetime.now()),

)

example_info = Recipe(
    Info,
    runner=foreign_key(example_runner),
    attribute_name=Info.ATTRIBUTE_NAME_CHOICES[0][0],
    attribute_value="piscis",
)

example_device = Recipe(
    Device,
    owner=foreign_key(example_runner),
)

example_activity_id = Recipe(
    Activity,
    id=1,
    runner=foreign_key(example_runner),
    created_at=make_aware(datetime.now()),
)

example_activity_no_id = Recipe(
    Activity,
    runner=foreign_key(example_runner),
    created_at=make_aware(datetime.now()),
)

example_report = Recipe(
    Report,
    activity=foreign_key(example_activity_id),
    created_at=make_aware(datetime.now()),
)

example_route = Recipe(
    Route,
    activity=foreign_key(example_activity_id),
    timestamp=make_aware(datetime.now()),
)
