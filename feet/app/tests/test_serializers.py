import io
from model_bakery import baker
import pytest
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

from app.models import (
    Runner,
    Info,
    Device,
    Activity,
    Report,
    Route,
)
from app.serializers import (
    RunnerSerializer,
    InfoSerializer,
    DeviceSerializer,
    ActivitySerializer,
    ReportSerializer,
    RouteSerializer,
    RouteNoActivitySerializer,
    RouteHeatMapSerializer,
)

# --- project ---

# fixtures to be seen for all test classes


@pytest.fixture
def runner_and_serializer():
    runner = baker.make_recipe("app.tests.example_runner")

    expected_runner_serializer = {
        "first_name": runner.first_name,
        "last_name": runner.last_name,
        "email": runner.email,
        "created_at": runner.created_at.isoformat(),
    }

    return [runner, expected_runner_serializer]


@pytest.fixture
def info_and_serializer():
    info = baker.make_recipe("app.tests.example_info")

    expected_info_serializer = {
        "runner": info.runner_id,
        "attribute_name": info.attribute_name,
        "attribute_value": info.attribute_value,
    }

    return [info, expected_info_serializer]


@pytest.fixture
def device_and_serializer():
    device = baker.make_recipe("app.tests.example_device")

    expected_device_serializer = {
        "model": device.model,
        "os": device.os,
        "search_engine": device.search_engine,
        "owner": device.owner_id,
    }

    return [device, expected_device_serializer]


@pytest.fixture
def activity_and_serializer_id():
    activity = baker.make_recipe("app.tests.example_activity_id")

    expected_activity_serializer = {
        "runner": activity.runner_id,
        "created_at": activity.created_at.isoformat(),
        "updated_at": activity.created_at.isoformat(),
        "activity_routes": [],
    }

    return [activity, expected_activity_serializer]


@pytest.fixture
def activity_and_serializer_no_id():
    activity = baker.make_recipe("app.tests.example_activity_no_id")

    expected_activity_serializer = {
        "runner": activity.runner_id,
        "created_at": activity.created_at.isoformat(),
        "updated_at": activity.created_at.isoformat(),
        "activity_routes": [],
    }

    return [activity, expected_activity_serializer]


@pytest.fixture
def report_and_serializer():
    report = baker.make_recipe("app.tests.example_report")

    expected_report_serializer = {
        "activity": report.activity_id,
        "created_at": report.created_at.isoformat(),
        "avg_speed": report.avg_speed,
        "max_speed": report.max_speed,
        "duration_of_activity": report.duration_of_activity,
        "calories_burned": report.calories_burned,
        "kilometers": report.kilometers,
    }

    return [report, expected_report_serializer]


@pytest.fixture
def route_and_serializer():
    route = baker.make_recipe("app.tests.example_route")

    expected_route_serializer = {
        "lat_coord": route.lat_coord,
        "lon_coord": route.lon_coord,
        "timestamp": route.timestamp.isoformat(),
        "activity": route.activity_id,
    }

    return [route, expected_route_serializer]


@pytest.fixture
def route_heatmap_and_serializer():
    route = baker.make_recipe("app.tests.example_route")
    expected_route_serializer = {
        "lat": route.lat_coord,
        "lng": route.lon_coord,
        "weight": 0.5,
    }

    return [route, expected_route_serializer]


# test classes


class TestRunnerSerializer:
    @pytest.mark.django_db
    def test_runner_serialization(self, runner_and_serializer):
        runner = runner_and_serializer[0]
        expected_serializer_data = runner_and_serializer[1]
        serializer_data = RunnerSerializer(runner).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_runner_deserialization(self, runner_and_serializer):
        runner = runner_and_serializer[0]
        content = JSONRenderer().render(runner_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = RunnerSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        runners = Runner.objects.all()

        assert runners.count() == 2
        assert runners.last().first_name == runner.first_name
        assert runners.last().last_name == runner.last_name


class TestInfoSerializer:
    @pytest.mark.django_db
    def test_info_serialization(self, info_and_serializer):
        info = info_and_serializer[0]
        expected_serializer_data = info_and_serializer[1]
        serializer_data = InfoSerializer(info).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_info_deserialization(self, info_and_serializer):
        driver = info_and_serializer[0]
        content = JSONRenderer().render(info_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = InfoSerializer(data=data)

        serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        infos = Info.objects.all()

        assert infos.count() == 2
        assert infos.last().runner_id == driver.runner_id
        assert infos.last().attribute_name == driver.attribute_name


class TestRouteSerializer:
    @pytest.mark.django_db
    def test_route_serialization(self, route_and_serializer):
        route = route_and_serializer[0]
        expected_serializer_data = route_and_serializer[1]
        serializer_data = RouteSerializer(route).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_route_heatmap_serialization(self, route_heatmap_and_serializer):
        route = route_heatmap_and_serializer[0]
        expected_serializer_data = route_heatmap_and_serializer[1]
        serializer_data = RouteHeatMapSerializer(route).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_route_deserialization(
        self,
        route_and_serializer,
    ):
        route = route_and_serializer[0]
        expected_serializer = route_and_serializer[1]
        content = JSONRenderer().render(expected_serializer)
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = RouteSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        routes = Route.objects.all()

        assert routes.count() == 2
        assert routes.last().lat_coord == route.lat_coord
        assert routes.last().lon_coord == route.lon_coord
        assert routes.last().activity_id == route.activity_id


class TestActivitySerializer:
    @pytest.mark.django_db
    def test_activity_serialization(
        self,
        activity_and_serializer_id,
    ):

        route = baker.make_recipe("app.tests.example_route")
        activity = activity_and_serializer_id[0]
        expected_serializer_data = activity_and_serializer_id[1]
        expected_serializer_data["activity_routes"].append(
            RouteNoActivitySerializer(route).data
        )
        serializer_data = ActivitySerializer(activity).data
        assert serializer_data["runner"] == expected_serializer_data["runner"]
        assert (
            serializer_data["created_at"] == expected_serializer_data["created_at"]
        )
        assert (
            serializer_data["activity_routes"][0]
            == expected_serializer_data["activity_routes"][0]
        )

    @pytest.mark.django_db
    def test_activity_deserialization(self, activity_and_serializer_no_id):
        activity = activity_and_serializer_no_id[0]
        expected_serializer = activity_and_serializer_no_id[1]
        content = JSONRenderer().render(expected_serializer)
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ActivitySerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        activities = Activity.objects.all()

        assert activities.count() == 2
        assert activities.last().created_at == activity.created_at
        assert activities.last().runner_id == activity.runner_id


class TestDeviceSerializer:
    @pytest.mark.django_db
    def test_device_serialization(self, device_and_serializer):
        device = device_and_serializer[0]
        expected_serializer_data = device_and_serializer[1]
        serializer_data = DeviceSerializer(device).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_device_deserialization(self, device_and_serializer):
        device = device_and_serializer[0]
        expected_serializer = device_and_serializer[1]
        content = JSONRenderer().render(expected_serializer)
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = DeviceSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        devices = Device.objects.all()

        assert devices.count() == 2
        assert devices.last().model == device.model
        assert devices.last().owner_id == device.owner_id


class TestReportSerializer:
    @pytest.mark.django_db
    def test_report_serialization(self, report_and_serializer):
        report = report_and_serializer[0]
        expected_serializer_data = report_and_serializer[1]
        serializer_data = ReportSerializer(report).data

        assert serializer_data == expected_serializer_data

    @pytest.mark.django_db
    def test_report_deserialization(self, report_and_serializer):
        report = report_and_serializer[0]
        content = JSONRenderer().render(report_and_serializer[1])
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = ReportSerializer(data=data)

        assert serializer.is_valid()
        assert serializer.errors == {}

        serializer.save()
        reports = Report.objects.all()

        assert reports.count() == 2
        assert reports.last().activity_id == report.activity_id
        assert reports.last().kilometers == report.kilometers
        assert reports.last().created_at == report.created_at
