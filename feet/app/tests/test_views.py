import pytest
from rest_framework import status
from model_bakery import baker
from app.serializers import (
    RunnerSerializer,
    InfoSerializer,
    DeviceSerializer,
    ActivitySerializer,
    ReportSerializer,
    RouteSerializer,
)

# --- project ---


class TestRunnerListApiView:
    @pytest.mark.django_db
    def test_runner_get(self, client):
        runner1 = baker.make_recipe("app.tests.example_runner")
        runner2 = baker.make_recipe(
            "app.tests.example_runner",
            id=2,
        )
        runner_list = []
        runner_list.append(RunnerSerializer(runner1).data)
        runner_list.append(RunnerSerializer(runner2).data)
        request = client.get("/api/runners/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == runner_list


class TestRunnerDetailApiView:
    @pytest.mark.django_db
    def test_detail_runner_get(self, client):
        runner = baker.make_recipe("app.tests.example_runner")
        id = runner.id
        request = client.get(f"/api/runners/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == RunnerSerializer(runner).data

    @pytest.mark.django_db
    def test_detail_no_runner_get(self, client):
        request = client.get("/api/runners/1/")
        assert request.status_code == status.HTTP_404_NOT_FOUND


class TestInfoListApiView:
    @pytest.mark.django_db
    def test_info_get(self, client):
        info1 = baker.make_recipe("app.tests.example_info")
        info2 = baker.make_recipe(
            "app.tests.example_info",
            id=2,
        )
        info_list = []
        info_list.append(InfoSerializer(info1).data)
        info_list.append(InfoSerializer(info2).data)
        request = client.get("/api/infos/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == info_list


class TestInfoDetailApiView:
    @pytest.mark.django_db
    def test_detail_info_get(self, client):
        info = baker.make_recipe("app.tests.example_info")
        id = info.id
        request = client.get(f"/api/infos/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == InfoSerializer(info).data

    @pytest.mark.django_db
    def test_detail_no_info_get(self, client):
        request = client.get("/api/infos/1/")
        assert request.status_code == status.HTTP_404_NOT_FOUND


class TestRouteListApiView:
    @pytest.mark.django_db
    def test_routes_get(self, client):
        route1 = baker.make_recipe("app.tests.example_route")
        route2 = baker.make_recipe(
            "app.tests.example_route",
            id=2,
        )
        route_list = []
        route_list.append(RouteSerializer(route1).data)
        route_list.append(RouteSerializer(route2).data)
        request = client.get("/api/routes/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == route_list


class TestRouteDetailApiView:
    @pytest.mark.django_db
    def test_detail_route_get(self, client):
        route = baker.make_recipe("app.tests.example_route")
        id = route.id
        request = client.get(f"/api/routes/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == RouteSerializer(route).data

    @pytest.mark.django_db
    def test_detail_no_route_get(self, client):
        request = client.get("/api/routes/1/")
        assert request.status_code == status.HTTP_404_NOT_FOUND


class TestRouteHeatmapApiView:
    @pytest.mark.django_db
    def test_heatmap_get(self, client):
        route = baker.make_recipe("app.tests.example_route")
        date = route.timestamp.date()
        request = client.get(f"/api/heatmap/?date={date}")
        assert request.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_heatmap_no_date_get(self, client):
        request = client.get("/api/heatmap/")
        assert request.status_code == status.HTTP_401_UNAUTHORIZED


class TestDeviceListApiView:
    @pytest.mark.django_db
    def test_device_get(self, client):
        device1 = baker.make_recipe("app.tests.example_device")
        device2 = baker.make_recipe(
            "app.tests.example_device",
            id=2,
        )
        device_list = []
        device_list.append(DeviceSerializer(device1).data)
        device_list.append(DeviceSerializer(device2).data)
        request = client.get("/api/devices/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == device_list


class TestDeviceDetailApiView:
    @pytest.mark.django_db
    def test_detail_device_get(self, client):
        device = baker.make_recipe("app.tests.example_device")
        id = device.id
        request = client.get(f"/api/devices/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == DeviceSerializer(device).data

    @pytest.mark.django_db
    def test_detail_no_device_get(self, client):
        request = client.get("/api/devices/1/")
        assert request.status_code == status.HTTP_404_NOT_FOUND


class TestActivityListApiView:
    @pytest.mark.django_db
    def test_activity_get(self, client):
        activity1 = baker.make_recipe("app.tests.example_activity_id")
        activity2 = baker.make_recipe(
            "app.tests.example_activity_id",
            id=2,
        )
        activity_list = []
        activity_list.append(ActivitySerializer(activity1).data)
        activity_list.append(ActivitySerializer(activity2).data)
        request = client.get("/api/activities/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == activity_list


class TestActivityDetailApiView:
    @pytest.mark.django_db
    def test_detail_activity_get(self, client):
        activity = baker.make_recipe("app.tests.example_activity_id")
        id = activity.id
        request = client.get(f"/api/activities/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ActivitySerializer(activity).data

    @pytest.mark.django_db
    def test_detail_no_client_payment_get(self, client):
        request = client.get("/api/activities/1/")
        assert request.status_code == status.HTTP_404_NOT_FOUND


class TestReportListApiView:
    @pytest.mark.django_db
    def test_report_get(self, client):
        report1 = baker.make_recipe("app.tests.example_report")
        report2 = baker.make_recipe(
            "app.tests.example_report",
            id=2,
        )
        report_list = []
        report_list.append(ReportSerializer(report1).data)
        report_list.append(ReportSerializer(report2).data)
        request = client.get("/api/reports/")
        response = request.data

        assert request.status_code == status.HTTP_200_OK
        assert response == report_list


class TestReportDetailApiView:
    @pytest.mark.django_db
    def test_detail_report_get(self, client):
        report = baker.make_recipe("app.tests.example_report")
        id = report.id
        request = client.get(f"/api/reports/{id}/")
        response = request.data
        assert request.status_code == status.HTTP_200_OK
        assert response == ReportSerializer(report).data

    @pytest.mark.django_db
    def test_detail_no_report_get(self, client):
        request = client.get("/api/reports/1/")
        assert request.status_code == status.HTTP_404_NOT_FOUND


class TestLastActivityRouteView:
    @pytest.mark.django_db
    def test_last_activity_route_get(self, client):
        route = baker.make_recipe("app.tests.example_route")
        first_name = "runner_name"
        last_name = "runner_lastname"
        date = route.timestamp.date()
        request = client.get(
            f"/api/last-activity-route/?first_name={first_name}&last_name={last_name}&date={date}"
        )
        assert request.status_code == status.HTTP_200_OK

    @pytest.mark.django_db
    def test_last_activity_route_no_route_get(self, client):
        runner = baker.make_recipe("app.tests.example_runner")
        first_name = runner.first_name
        last_name = runner.last_name
        date = "2023-04-01"
        request = client.get(
            f"/api/last-activity-route/?first_name={first_name}&last_name={last_name}&date={date}"
        )
        assert request.status_code == status.HTTP_404_NOT_FOUND

    @pytest.mark.django_db
    def test_last_activity_route_bad_request_get(self, client):
        request = client.get("/api/last-activity-route/")
        assert request.status_code == status.HTTP_400_BAD_REQUEST
