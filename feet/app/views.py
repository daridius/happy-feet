from .models import (
    Runner,
    Info,
    Device,
    Activity,
    Report,
    Route,
)

from .serializers import (
    RunnerSerializer,
    InfoSerializer,
    DeviceSerializer,
    ActivitySerializer,
    ReportSerializer,
    RouteSerializer,
    RouteHeatMapSerializer,
    RouteNoActivitySerializer,
)
import json
from rest_framework import mixins
from rest_framework import generics
from django.shortcuts import render
from django.utils.dateparse import parse_date
from django.http import HttpResponse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

from rest_framework.decorators import api_view
from rest_framework.response import Response

# Create your views here.

# import random
# from rest_framework.decorators import api_view

# --- examples ---

# @api_view(["GET", "POST"])
# def index(request):
#     """
#     Displays a random integer between 0 and 100, or returns the div of a random
#     number between 0 and 100 by a given integer.
#     """
#     if request.method == "GET":
#         random_integer = random.randrange(101)
#         dict = {"random integer value": random_integer}
#         return Response(dict)

#     elif request.method == "POST":
#         input_integer = int(request.data["value"])
#         random_integer = random.randrange(101)
#         if input_integer == 0:
#             dict = {
#                 "random integer to divide": random_integer,
#                 "input integer": input_integer,
#                 "Error": "cannot divide a number by zero.",
#             }
#             return Response(dict, status=status.HTTP_400_BAD_REQUEST)
#         div = random_integer // input_integer
#         dict = {
#             "random integer to divide": random_integer,
#             "input integer": input_integer,
#             "floor division value": div,
#         }
#         return Response(dict, status=status.HTTP_200_OK)

# --- project ---

# Runner views


class RunnerListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Runner.objects.all()
    serializer_class = RunnerSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RunnerDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Runner.objects.all()
    serializer_class = RunnerSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Info views


class InfoListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Info.objects.all()
    serializer_class = InfoSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class InfoDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Info.objects.all()
    serializer_class = InfoSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Device views


class DeviceListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class DeviceDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Activity views


class ActivityListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ActivityDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Report views


class ReportListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ReportDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# Route views


class RouteListApiView(mixins.ListModelMixin, generics.GenericAPIView):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class RouteDetailApiView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Route.objects.all()
    serializer_class = RouteSerializer

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


# see heatmap of certain day
# example usage: api/heatmap/?date=2023-04-01
def heatmap(request):
    date_str = request.GET.get("date", "")
    print(date_str)
    date = parse_date(date_str)
    print(date)
    if not date:
        return HttpResponse("Unauthorized", status=401)
    queryset = Route.objects.filter(timestamp__date=date)
    serializer = RouteHeatMapSerializer(queryset, many=True)
    G_KEY = settings.G_KEY
    return render(
        request,
        "heatmap.html",
        {
            "points": json.loads(json.dumps(serializer.data)),
            "url": f"https://maps.googleapis.com/maps/api/js?key={G_KEY}&callback=initMap&libraries=visualization",
        },
    )


# Returns the last route of an activity taken place
# by a specific runner on a specific date
# example usage: api/last-activity-route/?first_name=Alberto&last_name=Carrera&date=2023-04-01
@api_view()
def last_activity_route(request):
    first_name = request.GET.get("first_name")
    last_name = request.GET.get("last_name")
    date = request.GET.get("date")

    if not first_name or not last_name or not date:
        return HttpResponse("Bad Request", status=400)

    try:
        runner = Runner.objects.get(first_name=first_name, last_name=last_name)
        parsed_date = timezone.datetime.strptime(date, "%Y-%m-%d").date()
        activity = Activity.objects.filter(
            runner=runner,
            created_at__date=parsed_date,
        ).latest("created_at")
        last_route = Route.objects.filter(
            activity=activity,
        ).latest("timestamp")

        serializer = RouteNoActivitySerializer(last_route)
        return Response(serializer.data)

    except ObjectDoesNotExist:
        return Response(
            "No route found for the given runner and date.", status=404
        )
