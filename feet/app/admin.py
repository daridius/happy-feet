from django.contrib import admin

from .models import (
    Runner,
    Info,
    Device,
    Activity,
    Report,
    Route,
)

# Register your models here.

# from .models import IntegerBox

# Register your models here.

# --- exmaples ---

# admin.site.register(IntegerBox)

# --- project ---

admin.site.register(Runner)
admin.site.register(Info)
admin.site.register(Device)
admin.site.register(Activity)
admin.site.register(Report)
admin.site.register(Route)
