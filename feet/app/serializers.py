from rest_framework import serializers
from rest_framework.serializers import SerializerMethodField

from .models import (
    Runner,
    Info,
    Device,
    Activity,
    Report,
    Route,
)


class RunnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Runner
        fields = [
            "first_name",
            "last_name",
            "email",
            "created_at",
        ]


class InfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Info
        fields = [
            "runner",
            "attribute_name",
            "attribute_value",
        ]


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = [
            "model",
            "os",
            "search_engine",
            "owner",
        ]


class ActivitySerializer(serializers.ModelSerializer):
    activity_routes = SerializerMethodField()

    class Meta:
        model = Activity
        fields = [
            "runner",
            "created_at",
            "updated_at",
            "activity_routes",
        ]

    def get_activity_routes(self, activity):
        activity_routes = Route.objects.filter(activity=activity.id)
        serializer = RouteNoActivitySerializer(activity_routes, many=True)
        return serializer.data


class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = [
            "activity",
            "created_at",
            "avg_speed",
            "max_speed",
            "duration_of_activity",
            "calories_burned",
            "kilometers",
        ]


class RouteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ["lat_coord", "lon_coord", "timestamp", "activity"]


class RouteNoActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Route
        fields = ["lat_coord", "lon_coord", "timestamp"]


class RouteHeatMapSerializer(serializers.ModelSerializer):
    lng = serializers.FloatField(source="lon_coord")
    lat = serializers.FloatField(source="lat_coord")
    weight = SerializerMethodField()

    class Meta:
        model = Route
        fields = ["lat", "lng", "weight"]

    def get_weight(self, route):
        return 0.5
