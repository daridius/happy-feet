from django.urls import path

from .views import (
    RunnerListApiView,
    RunnerDetailApiView,
    InfoListApiView,
    InfoDetailApiView,
    DeviceListApiView,
    DeviceDetailApiView,
    ActivityListApiView,
    ActivityDetailApiView,
    ReportListApiView,
    ReportDetailApiView,
    RouteListApiView,
    RouteDetailApiView,
    heatmap,
    last_activity_route,
)

urlpatterns = [
    # --- examples ---
    # path("", views.index, name="index"),
    # --- project ---
    path("runners/", RunnerListApiView.as_view()),
    path("runners/<int:pk>/", RunnerDetailApiView.as_view()),
    path("infos/", InfoListApiView.as_view()),
    path("infos/<int:pk>/", InfoDetailApiView.as_view()),
    path("devices/", DeviceListApiView.as_view()),
    path("devices/<int:pk>/", DeviceDetailApiView.as_view()),
    path("activities/", ActivityListApiView.as_view()),
    path("activities/<int:pk>/", ActivityDetailApiView.as_view()),
    path("reports/", ReportListApiView.as_view()),
    path("reports/<int:pk>/", ReportDetailApiView.as_view()),
    path("routes/", RouteListApiView.as_view()),
    path("routes/<int:pk>/", RouteDetailApiView.as_view()),
    path("heatmap/", heatmap, name="heatmap"),
    path("last-activity-route/", last_activity_route, name="last_activity_route"),
]
