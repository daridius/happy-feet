from django.db import models
from privacy.annotations import PrivacyAnnotation


class Runner(models.Model):
    first_name = models.CharField(max_length=400)
    last_name = models.CharField(max_length=400)
    email = models.EmailField(max_length=254)
    created_at = models.DateTimeField()
    
    class PrivacyMeta(PrivacyAnnotation):
        id = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        first_name = {"categories": ["user.name"], "subject": "runner", "retention_time": "1 year"}
        last_name = {"categories": ["user.name"], "subject": "runner", "retention_time": "1 year"}
        email = {"categories": ["user.contact.email"], "subject": "runner", "retention_time": "1 year"}
        created_at = {"categories": ["system.operations"], "subject": "runner", "retention_time": "1 year"}

    def __str__(self) -> str:
        full_name = self.first_name + " " + self.last_name
        return full_name


class Info(models.Model):
    WESTERN_ZODIAC = "WZ"
    CHINESE_ZODIAC = "CZ"
    EDUCATION = "ED"
    CAREER = "CAR"
    JOB = "JB"
    MARITAL_STATUS = "MST"
    FAVORITE_COLOR = "CLR"
    FAVORITE_ANIMAL = "AN"
    FAVORITE_SONG = "SNG"
    FAVORITE_HOBBIE = "HOB"

    ATTRIBUTE_NAME_CHOICES = [
        (WESTERN_ZODIAC, "Western zodiac sign"),
        (CHINESE_ZODIAC, "Chinese zodiac sign"),
        (EDUCATION, "Education"),
        (CAREER, "Career"),
        (JOB, "Job"),
        (MARITAL_STATUS, "Marital status"),
        (FAVORITE_COLOR, "Favorite color"),
        (FAVORITE_ANIMAL, "Favorite animal"),
        (FAVORITE_SONG, "Favorite song"),
        (FAVORITE_HOBBIE, "Favorite hobbie"),
    ]

    runner = models.ForeignKey(Runner, on_delete=models.CASCADE)
    attribute_name = models.CharField(
        max_length=3,
        choices=ATTRIBUTE_NAME_CHOICES,
        default="",
    )
    attribute_value = models.CharField(max_length=400)

    class PrivacyMeta(PrivacyAnnotation):
        id = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        runner = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        attribute_name = {"categories": ["user.profiling", "user.job_title", "user.social"], "subject": "runner", "retention_time": "1 year"}
        attribute_value = {"categories": ["user.profiling", "user.job_title", "user.social"], "subject": "runner", "retention_time": "1 year"}
        

    def __str__(self) -> str:
        return f"Runner Id: {self.runner_id}, Attribute: {self.attribute_name}"


class Device(models.Model):
    model = models.CharField(max_length=400)
    os = models.CharField(max_length=400)
    search_engine = models.CharField(max_length=400)
    owner = models.ForeignKey(Runner, on_delete=models.CASCADE)

    class PrivacyMeta(PrivacyAnnotation):
        id = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        owner = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        model = {"categories": ["user.device"], "subject": "runner", "retention_time": "1 year"}
        os = {"categories": ["user.device"], "subject": "runner", "retention_time": "1 year"}
        search_engine = {"categories": ["user.device"], "subject": "runner", "retention_time": "1 year"}
             
    def __str__(self) -> str:
        return f"Owner Id: {self.owner_id}, Model: {self.model}"


class Activity(models.Model):
    runner = models.ForeignKey(Runner, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class PrivacyMeta(PrivacyAnnotation):
        id = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        runner = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        created_at = {"categories": ["system.operations"], "subject": "runner", "retention_time": "1 year"}
        updated_at = {"categories": ["system.operations"], "subject": "runner", "retention_time": "1 year"}

    def __str__(self) -> str:
        return f"Runner Id: {self.runner_id}, Created at: {self.created_at}"


class Report(models.Model):
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    avg_speed = models.FloatField()
    max_speed = models.FloatField()
    duration_of_activity = models.FloatField()
    calories_burned = models.IntegerField()
    kilometers = models.FloatField()

    class PrivacyMeta(PrivacyAnnotation):
        id = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        activity = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        avg_speed = {"categories": ["user.telemetry"], "subject": "runner", "retention_time": "1 year"}
        max_speed = {"categories": ["user.telemetry"], "subject": "runner", "retention_time": "1 year"}
        duration_of_activity = {"categories": ["user.telemetry"], "subject": "runner", "retention_time": "1 year"}
        calories_burned = {"categories": ["user.telemetry"], "subject": "runner", "retention_time": "1 year"}
        kilometers = {"categories": ["user.telemetry"], "subject": "runner", "retention_time": "1 year"}

    def __str__(self) -> str:
        return f"Activity Id: {self.activity_id}, Created at: {self.created_at}"


class Route(models.Model):
    lat_coord = models.FloatField()
    lon_coord = models.FloatField()
    timestamp = models.DateTimeField()
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)

    class Meta:
        ordering = ["timestamp"]

    class PrivacyMeta(PrivacyAnnotation):
        id = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        activity = {"categories": ["user.unique_id"], "subject": "runner", "retention_time": "1 year"}
        lat_coord = {"categories": ["user.location"], "subject": "runner", "retention_time": "6 months"}
        lon_coord = {"categories": ["user.location"], "subject": "runner", "retention_time": "6 months"}
        timestamp = {"categories": ["system.operations"], "subject": "runner", "retention_time": "1 year"}


    def __str__(self) -> str:
        route_resume = (
            f"Activity Id: {self.activity_id}, Timestamp: {self.timestamp}"
        )
        return route_resume
