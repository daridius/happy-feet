from django.test import TestCase
from django.apps import apps
from privacy.data_taxonomy import TAXONOMY

def test_models_with_privacy_meta():
    data_categories=list(map(lambda x: x.fides_key,TAXONOMY.data_category))
    data_subject=list(map(lambda x: x.fides_key,TAXONOMY.data_subject))
   
    for app_config in apps.get_app_configs():
        if "django" not in app_config.name:
            models = app_config.get_models()
            for model in models:
                assert hasattr(model, 'PrivacyMeta')
                privacy_meta_class = getattr(model, 'PrivacyMeta')

                fields_names = privacy_meta_class.get_fields()

                for field_name in fields_names:
                    assert hasattr(privacy_meta_class, field_name)
                    field = getattr(privacy_meta_class, field_name)

                    assert field is not None
                    assert field["retention_time"] is not None
                    for category in field["categories"]:
                        assert category in data_categories
                    assert field["subject"] in data_subject
