from django.core.management.base import BaseCommand, CommandError
from privacy.data_taxonomy import *

class Command(BaseCommand):
    def handle(self, *args, **options):
        write_manifest("data_classification.yaml",v.dict(exclude_none=True),"test")