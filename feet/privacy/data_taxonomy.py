from fideslang import DEFAULT_TAXONOMY
from fideslang.models import DataCategory,DataSubject,Dataset,DatasetCollection,DatasetField
from fideslang.manifests import write_manifest
TAXONOMY = DEFAULT_TAXONOMY

new_data_subjects=[DataSubject(
            fides_key="runner",
            organization_fides_key="default_organization",
            name="Runner",
            description="A runner is an individual who engages in the activity of running for exercise, sport, or recreation .",
            is_default=True,
        ),]

TAXONOMY.data_subject +=new_data_subjects


## Este es un dataset de ejemplo. Utilicen las mismas clases (i.e Dataset, DatasetCollection, DatasetField) para armar su Dataset para la parte 3
## Este es un dataset de ejemplo. Utilicen las mismas clases (i.e Dataset, DatasetCollection, DatasetField) para armar su Dataset para la parte 3
v=Dataset(
            organization_fides_key=1,
            fides_key="test_sample_db_dataset",
            name="Sample DB Dataset",
            description="This is a Sample Database Dataset",
            data_categories=list(map(lambda x: x.fides_key,TAXONOMY.data_category)),
            collections=[
                DatasetCollection(
                    name="Runner",
                    fields=[
                        DatasetField(
                            name="id",
                            description="User's ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="first_name",
                            description="User's first name",
                            data_categories=["user.name"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="last_name",
                            description="User's last name",
                            data_categories=["user.name"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="email",
                            description="User's Email",
                            data_categories=["user.contact.email"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="created_at",
                            description="Data creation date",
                            data_categories=["system.operations"],
                            retention="1 years",
                        ),
                    ],
                ),
                DatasetCollection(
                    name="Info",
                    fields=[
                        DatasetField(
                            name="id",
                            description="Info ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="runner",
                            description="Runner identificator",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="attribute_name",
                            description="Type of attribute",
                            data_categories=["user.profiling", "user.job_title", "user.social"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="attribute_value",
                            description="Value of the attribute",
                            data_categories=["user.profiling", "user.job_title", "user.social"],
                            retention="1 years",
                        ),
                    ],
                ),
                DatasetCollection(
                    name="Device",
                    fields=[
                        DatasetField(
                            name="id",
                            description="Device ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="owner",
                            description="Device owner ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="model",
                            description="Device's model",
                            data_categories=["user.device"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="os",
                            description="Device's operative system",
                            data_categories=["user.contact.device"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="search_engine",
                            description="Device's used search engine",
                            data_categories=["system.device"],
                            retention="1 years",
                        ),
                    ],
                ),
                DatasetCollection(
                    name="Activity",
                    fields=[
                        DatasetField(
                            name="id",
                            description="Activity ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="runner",
                            description="User doing the activity",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="created_at",
                            description="DB value creation date",
                            data_categories=["system.operations"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="updated_at",
                            description="DB value update date",
                            data_categories=["system.operations"],
                            retention="1 years",
                        ),
                    ],
                ),
                DatasetCollection(
                    name="Report",
                    fields=[
                        DatasetField(
                            name="id",
                            description="Report ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="activity",
                            description="Activity associated",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="avg_speed",
                            description="Activity average speed",
                            data_categories=["user.telemetry"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="max_speed",
                            description="Activity max. speed",
                            data_categories=["user.telemetry"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="duration_of_activity",
                            description="Activity duration",
                            data_categories=["system.telemetry"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="calories_burned",
                            description="Calories burned",
                            data_categories=["system.telemetry"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="kilometers",
                            description="Kilometers traveled",
                            data_categories=["system.telemetry"],
                            retention="1 years",
                        ),
                    ],
                ),
                DatasetCollection(
                    name="Route",
                    fields=[
                        DatasetField(
                            name="id",
                            description="Route ID",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="activity",
                            description="Activity related",
                            data_categories=["user.unique_id"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="lat_coord",
                            description="Latitude",
                            data_categories=["user.location"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="lon_coord",
                            description="Longitude",
                            data_categories=["user.location"],
                            retention="1 years",
                        ),
                        DatasetField(
                            name="timestamp",
                            description="Time related to the route",
                            data_categories=["system.operations"],
                            retention="1 years",
                        ),
                    ],
                )
            ],
        )

# write_manifest("data_classification.yaml",v.dict(exclude_none=True),"test")
